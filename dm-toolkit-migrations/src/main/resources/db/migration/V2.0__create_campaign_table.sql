CREATE TABLE public.campaign (
  id      UUID         NOT NULL         DEFAULT gen_random_uuid(),
  name    VARCHAR(128) NOT NULL,
  created TIMESTAMP    NOT NULL         DEFAULT now(),
  updated TIMESTAMP    NOT NULL         DEFAULT now(),
  CONSTRAINT PK_campaign PRIMARY KEY (id)
);

CREATE TRIGGER update_campaign_updated BEFORE UPDATE
  ON public.campaign FOR EACH ROW EXECUTE PROCEDURE update_updated_column();