CREATE TABLE public.campaign_player_character (
  id          UUID         NOT NULL         DEFAULT gen_random_uuid(),
  name        VARCHAR(128) NOT NULL,
  campaign_id UUID         NOT NULL,
  created     TIMESTAMP    NOT NULL         DEFAULT now(),
  updated     TIMESTAMP    NOT NULL         DEFAULT now(),
  CONSTRAINT PK_campaign_player_character PRIMARY KEY (id),
  CONSTRAINT FK_campaign_player_character_campaign FOREIGN KEY (campaign_id) REFERENCES campaign (id)
);

CREATE TRIGGER update_campaign_player_character_updated
BEFORE UPDATE
  ON public.campaign_player_character
FOR EACH ROW EXECUTE PROCEDURE update_updated_column();