CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE OR REPLACE FUNCTION update_updated_column()
  RETURNS TRIGGER AS $$
BEGIN
  NEW.updated = now() AT TIME ZONE 'utc';
  RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';