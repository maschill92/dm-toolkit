import React from 'react';
import {render} from 'react-dom';

render(
    <a href="http://www.google.com">Google</a> ,
    document.getElementById('root')
);