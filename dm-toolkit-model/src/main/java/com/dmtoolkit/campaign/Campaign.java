package com.dmtoolkit.campaign;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Entity
public class Campaign {
    @Id
    @GeneratedValue
    private UUID id;

    private String name;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "campaign")
    private Set<CampaignPlayerCharacter> campaignPlayerCharacters;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<CampaignPlayerCharacter> getCampaignPlayerCharacters() {
        return campaignPlayerCharacters;
    }

    public void setCampaignPlayerCharacters(Set<CampaignPlayerCharacter> campaignPlayerCharacters) {
        this.campaignPlayerCharacters = campaignPlayerCharacters;
    }
}
