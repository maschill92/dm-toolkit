package com.dmtoolkit.campaign;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface CampaignRepository extends PagingAndSortingRepository<Campaign, UUID> {
}