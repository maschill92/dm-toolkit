package com.dmtoolkit.campaign;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface CampaignPlayerCharacterRepository extends PagingAndSortingRepository<CampaignPlayerCharacter, UUID> {
}
